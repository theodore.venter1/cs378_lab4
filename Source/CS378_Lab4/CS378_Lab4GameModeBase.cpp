// Copyright Epic Games, Inc. All Rights Reserved.


#include "CS378_Lab4GameModeBase.h"
#include "Lab4Character.h"

// Class'/Game/Blueprints/Lab4CharacterBP.Lab4CharacterBP_C'
ACS378_Lab4GameModeBase::ACS378_Lab4GameModeBase()
{
	// PlayerControllerClass = ALab4PlayerController::StaticClass();
	static ConstructorHelpers::FObjectFinder<UClass> CharacterBPClass(TEXT("Class'/Game/Blueprints/Lab4CharacterBP.Lab4CharacterBP_C'"));
	if (CharacterBPClass.Object)
	{
		UClass* CharacterBP = (UClass*)CharacterBPClass.Object;
		DefaultPawnClass = CharacterBP;
	}
	else
	{
		DefaultPawnClass = ALab4Character::StaticClass();
	}
}