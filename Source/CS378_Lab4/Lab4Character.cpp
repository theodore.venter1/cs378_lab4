// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4Character.h"

// Sets default values
ALab4Character::ALab4Character()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// per lab handout
	n = 3;
	

}

void ALab4Character::ApplyMovement(float value)
{
	AddMovementInput(GetActorForwardVector(), value);
}

void ALab4Character::ApplyStrafe(float value)
{
	AddMovementInput(GetActorRightVector(), value);
}

void ALab4Character::BeginInteraction()
{
	GetWorld()->GetTimerManager().SetTimer(InteractTimerHandle, this, &ALab4Character::EndInteraction, n, false);
	GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Green, FString::Printf(TEXT("Interaction began")));
}

void ALab4Character::EndInteraction()
{
	CurrentActionState = ECharacterActionStateEnum::IDLE;
	GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Green, FString::Printf(TEXT("Interaction ended")));
}

// Called when the game starts or when spawned
void ALab4Character::BeginPlay()
{
	Super::BeginPlay();
	
}

bool ALab4Character::CanPerformAction(ECharacterActionStateEnum updatedAction)
{
	switch(CurrentActionState)
	{
	case ECharacterActionStateEnum::JUMP:
		if (updatedAction == ECharacterActionStateEnum::INTERACT)
			return false;
		return true;
	case ECharacterActionStateEnum::MOVE:
		if (updatedAction == ECharacterActionStateEnum::INTERACT)
			return false;
		return true;
	case ECharacterActionStateEnum::INTERACT:
		return false;
	case ECharacterActionStateEnum::IDLE:
		return true;
	}
	return false;	
}

void ALab4Character::UpdateAction(ECharacterActionStateEnum newAction)
{
	// "Player cannot perform any other action while in Interact"
	if (CurrentActionState == ECharacterActionStateEnum::INTERACT)
	{
		return;
	}
	
	if (newAction == ECharacterActionStateEnum::IDLE)
	{
		if (GetVelocity().IsNearlyZero())
		{
			CurrentActionState = ECharacterActionStateEnum::IDLE;
		} else
		{
			CurrentActionState = ECharacterActionStateEnum::MOVE;
		}
	} else
	{
		CurrentActionState = newAction;
	}
}

// Called every frame
void ALab4Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ALab4Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

